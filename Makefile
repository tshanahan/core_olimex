#/usr/bin/make -f
ifeq ($(strip $(MACHINE)),)
    $(error MACHINE variable not set. Usage: make MACHINE=olimex-a20-olinuxino-lime2)
endif

.SUFFIXES:

.PHONY: all gadget kernel ubuntu-core-image clean-gadget clean-kernel clean-ubuntu-core-image
all: gadget kernel ubuntu-core-image
clean: clean-gadget clean-kernel clean-ubuntu-core-image

#================================== GADGET =======================================
gadget: $(MACHINE)/gadget-snap/$(MACHINE)*.snap
$(MACHINE)/gadget-snap/$(MACHINE)*.snap:
	@echo "Building Gadget Snap"
	@cd $(MACHINE)/gadget-snap/ && sudo snapcraft

clean-gadget:
	@echo "Removing Gadget Snap"
	@cd $(MACHINE)/gadget-snap/ && sudo snapcraft clean
	@rm -f $(MACHINE)/gadget-snap/$(MACHINE)*.snap 

#================================== KERNEL =======================================

kernel: $(MACHINE)/kernel-snap/$(MACHINE)*.snap
$(MACHINE)/kernel-snap/$(MACHINE)*.snap:
	@echo "Building Kernel Snap"
	@cd $(MACHINE)/kernel-snap/ && sudo snapcraft --target-arch armhf

clean-kernel:
	@echo "Removing Kernel Snap"
	@rm -f $(MACHINE)/kernel-snap/$(MACHINE)*.snap

#================================== UBUNTU-IMAGE =======================================

ubuntu-core-image: gadget kernel $(MACHINE)/*.img
$(MACHINE)/*.img: 
	@echo "Building Ubuntu Core Image"
	@ubuntu-image -c stable -O imagefile --extra-snaps $(MACHINE)/kernel-snap/$(MACHINE)*.snap --extra-snaps $(MACHINE)/gadget-snap/$(MACHINE)*.snap \
            model/Olimex-lime2.model
	@mv imagefile $(MACHINE)/

clean-ubuntu-core-image:
	@echo "Removing Ubuntu Core Image"
	@rm -rf $(MACHINE)/imagefile/
