# Olimex A20-OlinuXino-Lime2 Gadget Snap

This repository contains the source for an Ubuntu Core gadget snap
for the Olimex and A20-OlinuXino-Lime2 and A20-OlinuXino-Lime2-eMMC. Building it
with snapcraft will automatically pull, configure, patch and build the git.denx.de/u-boot.git
upstream source for `A20-OLinuXino-Lime2_defconfig` and produce a bootable gadget
snap with the resulting binaries.

## Gadget Snaps

Gadget snaps are a special type of snaps that contain device specific support
code and data. You can read more about them in the snapd wiki
https://github.com/snapcore/snapd/wiki/Gadget-snap

## Building

*NOTE: this gadget snap can only build under 17.10 (artful), the sunxi u-boot build
has a hard requirement on gcc6!*

### Natively on armhf

Create an artful (17.10) chroot, install snapcraft and git inside it, chroot into it
and clone this tree.

To build the gadget snap locally on a native armhf system just rename snapcraft.yaml to 
snapcraft.yaml and run `snapcraft` in the toplevel of the tree.

### Cross on x86 systems

Create an artful (17.10) chroot, install snapcraft and git inside it, chroot into it
and clone this tree.

Run `snapcraft` to build gadget snap for armhf systems

Note:
Ubuntu cross compiler (arm-linux-gnueabihf- v5.3.1) is having bug it sometime gives compilation error 
while building u-boot. In that case download linaro cross-toolchain and update CROSSCOMPILE
flag in snapcraft.yaml (provide the absolute path of binary).
Dowload Linaro Cross-Toolchain using
`wget https://releases.linaro.org/components/toolchain/binaries/5.3-2016.02/arm-linux-gnueabihf/gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf.tar.xz` 

