# Olimex A20-OlinuXino-Lime2 Kernel Snap

This repository contains the source for an Ubuntu Core kernel snap
for the Olimex A20-OlinuXino-Lime2. Building it with snapcraft will 
automatically pull, configure, and build the local linux kernel v4.10.10
`sunxi_defconfig` and produce a bootable kernel snap with the resulting binaries.

## kernel Snaps

The kernel snap is responsible for defining the Linux kernel that will run in a
snap-based system. The correct kernel snap for a given system is selected via the 
model assertion, produced and signed by the device's brand account before the image 
is built.You can read more about them in the snapd wiki
https://forum.snapcraft.io/t/the-kernel-snap/697


### Natively on armhf

Create an artful (17.10) chroot, install snapcraft and git inside it, chroot into it
and clone this tree.

To build the kernel snap locally on a native armhf system run `snapcraft` in the 
toplevel of the tree.

### Cross on x86 systems

Create an artful (17.10) chroot, install snapcraft and git inside it, chroot into it
and clone this tree.

Run `snapcraft --target-arch armhf` to build kernel snap for armhf systems

