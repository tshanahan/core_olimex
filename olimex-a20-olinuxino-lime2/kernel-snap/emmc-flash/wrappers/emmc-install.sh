#!/bin/bash
#
# This is script could be used to transfer filesystem from MMC to eMMC.

#set -e

export LD_LIBRARY_PATH="$SNAP/usr/lib/ipsec/plugins:$SNAP/usr/lib/ipsec:$LD_LIBRARY_PATH"

#Exported Variables
  FWENVCMD=fw_printenv
  FWENVFILE=$SNAP_COMMON/sd_uboot.env.in
  RSYNCBIN=$SNAP/bin/rsync
  UBOOTEMMCFILE=/boot/uboot/uboot-emmc.env
  EMMCUBOOTBIN=/writable/system-data/snap/olimex-a20-olinuxino-lime2/x1/u-boot-sunxi-with-spl-emmc.bin
  MNTPOINTPARTITION=/mnt

# Set default values
  MMC_DEVICE=${MMC:="/dev/mmcblk0"}
  EMMC_DEVICE=${EMMC:="/dev/mmcblk1"}

  function check_mounted() {
    [ -z $1 ] && return

    line=$(df | grep "$1")
    [ -z "$line" ] && return

    echo $(echo $line | awk '{ print $6 }')
 }

  function print_with_color() {
      echo -e "\e[33m"$1"\e[0m"
  }

# Check for root
  print_with_color "Checking permissions..."
  [ $(id -u) -ne 0 ] && echo "This script must be run as root!" && exit 1

# Check if MMC and eMMC are present
  print_with_color "Checking MMC device..."
  [ ! -e $MMC_DEVICE ] &&
     echo "MMC device \"$MMC_DEVICE\" is missing!" &&
     exit 1

  print_with_color "Checking eMMC device..."
  [ ! -e $EMMC_DEVICE ] &&
     echo "eMMC device \"$EMMC_DEVICE\" is missing!" &&
     exit 1
	 
#Section 1 : Updating Load_Device Uboot Environment Variable and Uboot image for EMMC 
  print_with_color "Fetching SD card Environment Variables..."
  $FWENVCMD > $FWENVFILE 

  FWENVFILESIZE=$(stat -c%s "$FWENVFILE")
  [ $FWENVFILESIZE -eq 0 ] && echo "Environment variables are not fetched successfully" && exit 1

  line=$(cat $FWENVFILE | grep "load_device=")
  [ -z $line ] && echo "Failed to load_device info from uboot env file" && exit 1

  print_with_color "Updating \"load_device\" Environment Variables..."
  sed -i "s/load_device=0:1/load_device=1:1/" "$FWENVFILE" > /dev/null 2>&1

  print_with_color "Building Uboot Environment Image for EMMC..."
  mkenvimage -r -s 131072 -o $UBOOTEMMCFILE $FWENVFILE > /dev/null 2>&1

  EMMCENVFILESIZE=$(stat -c%s "$UBOOTEMMCFILE")
  [ $EMMCENVFILESIZE -eq 0 ] && echo "Uboot Environment Image for EMMC is not build successfully" && exit 1

  rm $FWENVFILE
# Section 1 Ends

# Section 2: Read current partition table and copying EMMC uboot environment image
  # Read format table of the MMC
    print_with_color "Reading $MMC_DEVICE partition table.."
    fdisk_output=$(fdisk $MMC_DEVICE -l | grep "^$MMC_DEVICE")

    print_with_color "Reading $EMMC_DEVICE partition table.."
    fdisk_output_emmc=$(fdisk $EMMC_DEVICE -l | grep "^$EMMC_DEVICE")
    partitions_count=$(echo "$fdisk_output_emmc" | wc -l)
    [ $partitions_count -eq 2 ] && echo "$EMMC_DEVICE is already partitioned! Clean it!" && exit 1

  # Make sure partition table on eMMC is not mounted and erased
    print_with_color "Erasing eMMC partition table..."

  # Clearing the EMMC disk  
    dd if="/dev/zero" of=$EMMC_DEVICE bs=1M count=3500 > /dev/null 2>&1
  
  # Copying the EMMC uboot image from gadget snap into EMMC at offset 8192 
    dd if=$EMMCUBOOTBIN of=/dev/mmcblk1 bs=1024 seek=8 > /dev/null 2>&1
#Section 2 Ends

#Section 3 : Create and Build EMMC Boot partition
  # Create Boot Partition 
    print_with_color "Creating EMMC Boot partition..."
    part=1
    partition=$(echo "$fdisk_output" | head -n $part | tail -n 1)
    partition=${partition/\*/ }

  # Check target fs
    fs=$(fsck -NT $MMC_DEVICE"p"$part | awk '{ print $5}' | \
          awk -F'.' '{ print $2 }')
    [ -z $fs ] && echo "Unknown target filesystem! for $MMC_DEVICE"p"$part" && exit 1
   
  # First read partiton start/end sector
    start=2048
    end=264191
    type=$(echo $partition | awk '{print $6}')

    print_with_color "Creating partition: $part"

    fdisk $EMMC_DEVICE > /dev/null 2>&1 << __EOF__
n
p
$part
$start
$end
t
$type
a
$part
w
__EOF__

  # Make file system
    print_with_color "Formating partition $part to $fs..."
    mkfs.$fs $EMMC_DEVICE"p"$part > /dev/null 2>&1

  # Label the boot partition as "system-boot"
    fatlabel $EMMC_DEVICE"p"$part "system-boot" > /dev/null 2>&1

  # Check if eMMC drive is mounted/busy
    if  mount | grep -q $EMMC_DEVICE"p"$part ; then 
        echo "$EMMC_DEVICE"p"$part is already mounted! Unmounting it"
        umount $EMMC_DEVICE"p"$part > /dev/null 2>&1
    fi

  # Check if mountpartition(/mnt) is mounted/busy
    if  mount | grep -q $MNTPOINTPARTITION; then 
        echo "$MNTPOINTPARTITION is already mounted! Unmounting it"
        umount $MNTPOINTPARTITION > /dev/null 2>&1
    fi

    mount $EMMC_DEVICE"p"$part $MNTPOINTPARTITION > /dev/null 2>&1

    if  mount | grep -q $EMMC_DEVICE"p"$part ; then 
        echo "$EMMC_DEVICE"p"$part is successfully mounted on $MNTPOINTPARTITION"
    else
        echo "$EMMC_DEVICE"p"$part is not successfully mounted on $MNTPOINTPARTITION! Exiting"
        exit 1
    fi
	
  # Check Mount point for boot partition in SD card
    mount_point=$(check_mounted $MMC_DEVICE"p"$part)

  # Copy files
    print_with_color "Copying files..."
    $RSYNCBIN -ah --info=progress2 $mount_point/* $MNTPOINTPARTITION

    umount $EMMC_DEVICE"p"$part > /dev/null 2>&1
#Section 3 Ends

#Section 3 : Create and Build EMMC Writable partition
  # Create Writable Partition 
    print_with_color "Creating EMMC Writable partition..."
    part=2
    partition=$(echo "$fdisk_output" | head -n $part | tail -n 1)
    partition=${partition/\*/ }

  # Check target fs
    fs=$(fsck -NT $MMC_DEVICE"p"$part | awk '{ print $5}' | \
        awk -F'.' '{ print $2 }')
    [ -z $fs ] && echo "Unknown target filesystem! for $MMC_DEVICE"p"$part" && exit 1
  
  # First read partiton start/end sector
    start=264192
    end=7553023
    type=$(echo $partition | awk '{print $6}')

    fdisk $EMMC_DEVICE > /dev/null 2>&1 << __EOF__
n
p
$part
$start
$end
t
$part
$type
w
__EOF__

  # Make file system
    print_with_color "Formating partition $part to $fs..."
    mkfs.$fs $EMMC_DEVICE"p"$part > /dev/null 2>&1

  # Label the writable partition as "writable"
    e2label $EMMC_DEVICE"p"$part "writable" > /dev/null 2>&1

  # Check if eMMC drive is mounted/busy
    if  mount | grep -q $EMMC_DEVICE"p"$part ; then 
        echo "$EMMC_DEVICE"p"$part is already mounted! Unmounting it"
        umount $EMMC_DEVICE"p"$part > /dev/null 2>&1
    fi

  # Check if mountpartition(/mnt) is mounted/busy
    if  mount | grep -q $MNTPOINTPARTITION; then 
        echo "$MNTPOINTPARTITION is already mounted! Unmounting it"
        umount $MNTPOINTPARTITION > /dev/null 2>&1
    fi

    mount $EMMC_DEVICE"p"$part $MNTPOINTPARTITION > /dev/null 2>&1

    if  mount | grep -q $EMMC_DEVICE"p"$part ; then 
        echo "$EMMC_DEVICE"p"$part is successfully mounted on $MNTPOINTPARTITION"
    else
        echo "$EMMC_DEVICE"p"$part is not successfully mounted on $MNTPOINTPARTITION! Exiting"
        exit 1
    fi

  # Check Mount point for writable partition in SD card
    mount_point=$(check_mounted $MMC_DEVICE"p"$part)

  # Copy files
    print_with_color "Copying files..."
    $RSYNCBIN -ah --info=progress2 $mount_point/* $MNTPOINTPARTITION

    umount $EMMC_DEVICE"p"$part > /dev/null 2>&1

#Section 4 Ends
  print_with_color "Finished! Shutting now"
  shutdown -hP now
  exit 0

