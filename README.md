This readme document provide commands to make gadget snap, kernel snap and ubuntu core image for Olimex A20-OlinuXIno-lime2 platform board.

Commands
a) To create gadget snap only
    ================================================
     make MACHINE=olimex-a20-olinuxino-lime2 gadget
    ================================================

b) To create kernel snap only
    ================================================
     make MACHINE=olimex-a20-olinuxino-lime2 kernel
    ================================================

c) To create ubuntu core image
    =============================================================
     make MACHINE=olimex-a20-olinuxino-lime2 ubuntu-core-image 
			or
     make MACHINE=olimex-a20-olinuxino-lime2 all 
			or
     make MACHINE=olimex-a20-olinuxino-lime2 
    =============================================================

d) To remove gadget snap only
    =====================================================
     make MACHINE=olimex-a20-olinuxino-lime2 clean-gadget
    =====================================================

e) To remove kernel snap only
    =====================================================
     make MACHINE=olimex-a20-olinuxino-lime2 clean-kernel
    =====================================================

f) To remove gadget snap only
    ================================================================
     make MACHINE=olimex-a20-olinuxino-lime2 clean-ubuntu-core-image
    ================================================================

g) To remove kernel snap, gadget snap and ubuntu-core image
    ================================================================
     make MACHINE=olimex-a20-olinuxino-lime2 clean
    ================================================================
